# Guide d'utilisation
### Structure du dossier
| Dossier / Fichier | Contenu |
| ------ | ------ |
| readme.txt | Fichier contenant un guide d'utilisation |
| video.mp4 | Fichier contenant la vidéo de démonstration |
| rapport.odt | Fichier contenant le rapport du TP |
| code| Dossier contenant le code de l'application Web |
----
### Installation du framework Angular
- ouvrir un nouvel onglet dans le navigateur de votre choix
- naviguer vers le site de notejs "https://nodejs.org/en/"
- choisir et télécharger l'installateur adapté à votre système d'exploitation
- installer nodejs à l'aide de l'installateur téléchargé
- ouvrir un shell dans un terminal
- naviguer vers le dossier "code/rennes-metropole" de l'application Web
- exécuter la commande "npm install -g @angular/cli" afin de lancer l'installation d'Angular
----

### Lancement de l'application Web
- ouvrir un shell dans un terminal
- naviguer vers le dossier "code/rennes-metropole" de l'application Web
- exécuter la commande "npm i" afin d'installer les dépendances non installées de l'application Web
- exécuter la commande "npm serve --open" afin de lancer l'application Web
----

### Utilisation de l'application Web
##### ouverture de la page d'accueil :
- repérer la barre de navigation en haut de la page
- cliquer sur le label "Accueil"

##### ouverture de la page de recherche :
- repérer la barre de navigation en haut de la page
- cliquer sur le label "Recherche"

##### recherche sans filtre :
- repérer la zone de recherche située au milieu de l'écran
- cliquer sur le bouton "Rechercher" pour effectuer la recherche
- repérer la zone de résultat située en dessous de la zone de recherche

##### recherche avec filtre de position :
- repérer la zone de recherche située au milieu de l'écran
- saisir la latitude de la position recherchée dans la zone de saisie correspondante
- saisir la longitude de la position recherchée dans la zone de saisie correspondante
- saisir la distance maximale souhaitée entre les résultats et la position recherchée dans la zone de saisie correspondante
- cliquer sur le bouton "Rechercher" pour effectuer la recherche
- repérer la zone de résultat située en dessous de la zone de recherche

##### recherche avec filtre de zone tarifaire :
- repérer la zone de recherche située au milieu de l'écran
- sélectionner la zone tarifaire recherchée dans le menu déroulant correspondant
- cliquer sur le bouton "Rechercher" pour effectuer la recherche
- repérer la zone de résultat située en dessous de la zone de recherche

##### recherche avec filtre du nombre de données affichées :
- repérer la zone de recherche située au milieu de l'écran
- saisir la nombre de données recherchées dans la zone de saisie correspondante
- cliquer sur le bouton "Rechercher" pour effectuer la recherche
- repérer la zone de résultat située en dessous de la zone de recherche