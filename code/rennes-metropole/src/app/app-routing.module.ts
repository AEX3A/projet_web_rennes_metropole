import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainSearchComponent } from './main-search/main-search.component';
import { MainResultComponent } from './main-result/main-result.component';
import { MainResultTableComponent } from './main-result-table/main-result-table.component';
import { MainResultStatsComponent } from './main-result-stats/main-result-stats.component';
import { MainHomeComponent } from './main-home/main-home.component';

/** Variable qui contient l'ensemble des routes de l'application Web */
const routes: Routes = [
  { path: 'home', component: MainHomeComponent }, 
  { path: 'search', component: MainSearchComponent,
    children: [
      { path: 'results', component: MainResultComponent,
        children: [
          { path: 'table', component: MainResultTableComponent },
          { path: 'stats', component: MainResultStatsComponent },
        ]},
    ]},
  /** route utilisée si l'url ne correspond à aucune route précisée précédement */
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/