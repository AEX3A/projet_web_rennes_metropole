import { Component, OnInit } from '@angular/core';
import { RequestDataService } from '../request-data.service';

@Component({
  selector: 'app-main-result-stats',
  templateUrl: './main-result-stats.component.html',
  styleUrls: [ './main-result-stats.component.css' ]
})
export class MainResultStatsComponent implements OnInit{
  nb_lignes;

  /** Constructeur du composant main-result-map */
  constructor(private request_data: RequestDataService) { }
  
  /** Méthode automatiquement appelée dès l'initialisation du composant */
  ngOnInit(): void {
      this.stats();
  }

  /** Méthode qui rempli le tableau de statistique avec les descriptions et les valeurs des statistiques */
  stats() {
    let json_data = this.request_data.get_request_result();
    let couleur = {nb_verte:0, verte_pourcent:0, nb_rouge:0, rouge_pourcent:0}
    let moyenne_prix = {moy_15:0, moy_30:0, moy_1h:0, moy_1h30:0, moy_2h:0, moy_3h:0, moy_4h:0}
    let line_number = json_data["records"].length;
    for(let i=0; json_data["records"][i]; i++) {
      if(json_data["records"][i]["fields"]["zone_tarif"]=="verte") {
        couleur.nb_verte += 1;
      } else if(json_data["records"][i]["fields"]["zone_tarif"]=="rouge") {
        couleur.nb_rouge += 1;
      }
      moyenne_prix.moy_15 += +json_data["records"][i]["fields"]["tarif_15"];
      moyenne_prix.moy_30 += +json_data["records"][i]["fields"]["tarif_30"];
      moyenne_prix.moy_1h += +json_data["records"][i]["fields"]["tarif_1h"];
      moyenne_prix.moy_1h30 += +json_data["records"][i]["fields"]["tarif_1h30"];
      moyenne_prix.moy_2h += +json_data["records"][i]["fields"]["tarif_2h"];
      moyenne_prix.moy_3h += +json_data["records"][i]["fields"]["tarif_3h"];
      moyenne_prix.moy_4h += +json_data["records"][i]["fields"]["tarif_4h"];
    }
    couleur.verte_pourcent = +(couleur.nb_verte/line_number*100);
    couleur.rouge_pourcent = +(couleur.nb_rouge/line_number*100);
    moyenne_prix.moy_15/=line_number, moyenne_prix.moy_30/=line_number, moyenne_prix.moy_1h/=line_number, moyenne_prix.moy_1h30/=line_number, moyenne_prix.moy_2h/=line_number, moyenne_prix.moy_3h/=line_number, moyenne_prix.moy_4h/=line_number;
    this.append_line_to_table("nombre de zones tarifaires vertes", couleur.nb_verte+" ("+couleur.verte_pourcent.toFixed(3)+" %)");
    this.append_line_to_table("nombre de zones tarifaires rouges", couleur.nb_rouge+" ("+couleur.rouge_pourcent.toFixed(3)+" %)");
    this.append_line_to_table("prix moyen pour un stationnement de 15 minutes", moyenne_prix.moy_15.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 30 minutes", moyenne_prix.moy_30.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 1 heure", moyenne_prix.moy_1h.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 1 heure 30", moyenne_prix.moy_1h30.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 2 heures", moyenne_prix.moy_2h.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 3 heures", moyenne_prix.moy_3h.toFixed(3)+" €");
    this.append_line_to_table("prix moyen pour un stationnement de 4 heures", moyenne_prix.moy_4h.toFixed(3)+" €");
  }

  /** Méthode qui ajoute une ligne au tableau en fonction des paramêtre utilisés lors de l'appel */
  append_line_to_table(description, valeur) {
      //Structure du TR
      var tr = document.createElement("tr");
      //Structure des TD
      var td1 = document.createElement("td");
      var td2 = document.createElement("td");
      //Structure des textes des TD
      var textnode1 = document.createTextNode(description);
      var textnode2 = document.createTextNode(valeur);
      td1.appendChild(textnode1);
      td2.appendChild(textnode2);
      //Ajout des TD dans le tr
      tr.appendChild(td1);
      tr.appendChild(td2);
      //Ajout du TR dans le tableau
      document.getElementById("fill").appendChild(tr);
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/