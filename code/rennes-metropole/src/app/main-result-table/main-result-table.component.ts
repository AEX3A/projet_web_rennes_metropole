import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RequestDataService } from '../request-data.service';

@Component({
  selector: 'app-main-result-table',
  templateUrl: './main-result-table.component.html',
  styleUrls: [ './main-result-table.component.css' ]
})
export class MainResultTableComponent implements OnInit {

  /**
   * Constructeur du composant main-result-table
   * @param http 
   * @param request_data 
   */
  constructor(private http: HttpClient, private request_data: RequestDataService) { }

  /** Méthode automatiquement appelée dès l'initialisation du composant */
  ngOnInit(): void {
      this.fill_table_with_results();
  }

  /** Méthode qui rempli le tableau avec les données de l'objet json du résultat de la requête */
  fill_table_with_results() {
    let json_data = this.request_data.get_request_result();
    for(let i = 0; i < json_data.records.length;i++){
      //Structure du TR
      var tr = document.createElement("tr");
      
      //Structure des TD
      var td0 = document.createElement("td");
      var td1 = document.createElement("td");
      var td2 = document.createElement("td");
      var td3 = document.createElement("td");
      var td4 = document.createElement("td");
      var td5 = document.createElement("td");
      var td6 = document.createElement("td");
      var td7 = document.createElement("td");
      var td8 = document.createElement("td");

      //Structure des textes des TD
      var textnode0 = document.createTextNode(json_data.records[i].fields.geo_point_2d != undefined ? json_data.records[i].fields.geo_point_2d[0].toFixed(3) + ' : ' + json_data.records[i].fields.geo_point_2d[1].toFixed(3) :"");
      var textnode1 = document.createTextNode(json_data.records[i].fields.zone_tarif);
      var textnode2 = document.createTextNode(json_data.records[i].fields.tarif_15+" €");
      var textnode3 = document.createTextNode(json_data.records[i].fields.tarif_30+" €");
      var textnode4 = document.createTextNode(json_data.records[i].fields.tarif_1h+" €");
      var textnode5 = document.createTextNode(json_data.records[i].fields.tarif_1h30+" €");
      var textnode6 = document.createTextNode(json_data.records[i].fields.tarif_2h+" €");
      var textnode7 = document.createTextNode(json_data.records[i].fields.tarif_3h+" €");
      var textnode8 = document.createTextNode(json_data.records[i].fields.tarif_4h+" €");
      td0.appendChild(textnode0);
      td1.appendChild(textnode1);
      td2.appendChild(textnode2);
      td3.appendChild(textnode3);
      td4.appendChild(textnode4);
      td5.appendChild(textnode5);
      td6.appendChild(textnode6);
      td7.appendChild(textnode7);
      td8.appendChild(textnode8);

      //Ajout des TD dans le tr
      tr.appendChild(td0);
      tr.appendChild(td1);
      tr.appendChild(td2);
      tr.appendChild(td3);
      tr.appendChild(td4);
      tr.appendChild(td5);
      tr.appendChild(td6);
      tr.appendChild(td7);
      tr.appendChild(td8);

      //Ajout du TR dans le tableau
      document.getElementById("fill").appendChild(tr);
    }
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/