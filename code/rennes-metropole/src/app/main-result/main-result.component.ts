import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-result',
  templateUrl: './main-result.component.html',
  styleUrls: [ './main-result.component.css' ]
})
export class MainResultComponent {

  /**
   * Constructeur du composant main-result
   * @param router
   * @param route 
   */
  constructor(private router: Router, private route: ActivatedRoute) { }
  
  /** Methode qui permet de naviguer vers l'url /search/results/table */
  navigate_to_table() {
    this.router.navigate(['table'], {relativeTo: this.route});
  }

  /** Methode qui permet de naviguer vers l'url /search/results/map */
  navigate_to_stats() {
    this.router.navigate(['stats'], {relativeTo: this.route});
  }

}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/