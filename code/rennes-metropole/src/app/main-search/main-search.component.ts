import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { RequestDataService } from '../request-data.service';

@Component({
  selector: 'app-main-search',
  templateUrl: './main-search.component.html',
  styleUrls: [ './main-search.component.css' ]
})
export class MainSearchComponent {
  /** Variable qui contient l'objet json correspondant aux valeurs des éléments du formulaire de recherche */
  form_data;

  /**
   * Constructeur de main-search
   * @param router 
   * @param route 
   * @param formBuilder 
   * @param request_data 
   */
  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClient, private formBuilder: FormBuilder, private request_data: RequestDataService) {
    /** Initialisation des valeurs de chaque élément du formulaire de recherche */
    this.form_data = this.formBuilder.group({
      latitude: null,
      longitude: null,
      distance: null,
      zone_tarifaire: 0,
      nb_lignes: 6762
    });
  }

  /** Méthode automatiquement appelée lors de l'appui sur le bouton "Rechercher" du formulaire de recherche */
  onSubmit(form_data) {
    /** Utilisation des données de la zone de recherche pour définir le contenu de la variable request_parameters du service */
    this.request_data.set_request_parameters(form_data);
    /** Utilisation de l'url retourné par le service pour effectuer la requête vers l'api */
    this.http.get(this.request_data.get_request_url()).subscribe((data) => {
      console.log(data);
      /** Modification de la valeur de la variable request_result du service avec l'objet json obtenu comme résultat de la requête */
      this.request_data.set_request_result(data);
      /** Navigation vers le composant dont l'url est "/search/results" */
      this.router.navigate(['results'], {relativeTo: this.route});
    });
  }
}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/