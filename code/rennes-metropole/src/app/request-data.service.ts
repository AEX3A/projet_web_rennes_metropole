import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestDataService {
  /** Variable qui contient l'objet json correspondant aux paramètres de la requête */
  request_parameters;
  /** Variable qui contient l'objet json correspondant au résultat de la requête */
  request_result;

  /** Constructeur du service search-data */
  constructor(private http: HttpClient) {
    this.request_parameters = null;
    this.request_result = null;
  }

  /** Methode qui modifie la valeur de la variable request_parameters par le contenu du paramètre parameter */
  set_request_parameters(parameters) {
    this.request_parameters = parameters;
  }

  /** Methode qui retourne la valeur de la variable request_parameters */
  get_request_parameters() {
    return this.request_parameters;
  }

  /** Methode qui retourne l'url de la requête en concatenant l'url de base et les différents filtres de recherche */
  get_request_url() {
    /** Variable qui contient l'url de base de la requête sous la forme d'une chaine de characters */
    let url = 'https://data.rennesmetropole.fr/api/records/1.0/search/?dataset=portions-de-voies-en-stationnement-payant-sur-la-ville-de-rennes&q=&facet=zone_tarif';
    if(this.request_parameters != null) {
      if(this.request_parameters.latitude != null && this.request_parameters.longitude != null && this.request_parameters.distance != null)
      url += "&geofilter.distance="+this.request_parameters.latitude+"%2C+"+this.request_parameters.longitude+"%2C+"+this.request_parameters.distance;
      if(this.request_parameters.zone_tarifaire != null)
      url += this.request_parameters.zone_tarifaire == "1" ? "&refine.zone_tarif=verte" : this.request_parameters.zone_tarifaire == "2" ? "&refine.zone_tarif=rouge" : "";
      if(this.request_parameters.nb_lignes != null)
      url += "&rows="+this.request_parameters.nb_lignes;
    }
    return url;
  }

  /** Methode qui modifie la valeur de la variable request_result par le contenu du paramètre result */
  set_request_result(result) {
    this.request_result = result;
  }

  /** Methode qui retourne la valeur de la variable request_result */
  get_request_result() {
    return this.request_result;
  }
}