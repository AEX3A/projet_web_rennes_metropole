import { Component } from '@angular/core';

@Component({
  selector: 'app-main-home',
  templateUrl: './main-home.component.html',
  styleUrls: [ './main-home.component.css' ]
})
export class MainHomeComponent {

  /** Constructeur du composant main-home */
  constructor() { }

}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/